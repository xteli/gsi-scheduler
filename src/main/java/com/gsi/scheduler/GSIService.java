/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsi.scheduler;

import com.gsi.lib.dao.GSISystem;
import com.gsi.lib.entities.GSIMandate;
import com.gsi.lib.util.Enum.ApprovalStatus;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 *
 * @author chineduojiteli
 */
public class GSIService {
    
    GSISystem gsiSystem = new GSISystem();
    
    private void checkMandateDueDate() {
        System.out.println(" ...Inside checkMandateDueDate()... ");
        System.out.println(" About retrieving undue GSI Mandates... ");
        List<GSIMandate> mandateList = gsiSystem.retrieveUndueGSIMandate();
        if (mandateList != null && !mandateList.isEmpty()) {
            mandateList.stream().filter(mandate -> mandate.getApproval().getApprovalStatus() == ApprovalStatus.Approved).forEach(gsiMandate -> {
                if (new SimpleDateFormat("yyyy-MM-dd").format(gsiMandate.getDateAvailable()).equals(new SimpleDateFormat("yyyy-MM-dd").format(new Date()))) {
                    gsiMandate.setIsAvailable(true);
                    gsiMandate.setMandateStatus(com.gsi.lib.util.Enum.MandateStatus.Active);
                    gsiSystem.updateEntity(gsiMandate);
                }
            });
        } else {
            System.out.println("No pending undue gsi mandates found ");
        }
        System.out.println(" ...Leaving checkMandateDueDate()... ");
    }
    
    public static void main(String[] args) throws Exception {
        GSIService gsiService = new GSIService();
        System.out.println(" ..: GSI Scheduler Service Started :.. ");
        int timeToWait = 120;
        timeToWait = Integer.parseInt(args[0]);
        
        System.out.println(" ..: Service has started :.. ");
        while (!Thread.currentThread().isInterrupted()) {
            gsiService.checkMandateDueDate();
            try {
                System.out.println(" Service is waiting for " + timeToWait + " seconds ");
                Thread.sleep(timeToWait * 1000);
            } catch (InterruptedException e) {
                break;
            }
        }
    }
}
